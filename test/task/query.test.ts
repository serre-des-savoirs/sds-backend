import { taskResolver } from "../../src/task/api/resolverTask";

describe("Task query", () => {
    test("test-1 taskResolver return all list of task", async () => {
        const repositoryTaskMem = {
            getAll: async () => ([
                {
                    id: "taskIdArtichoke-i",
                    type: "Artichoke",
                },
                {
                    id: "taskIdTomate-i",
                    type: "Tomate",
                }
            ]),
        }
        const res = await taskResolver(repositoryTaskMem);

        expect(res).toHaveLength(2);
        expect(res[0]).toEqual({
            id: "taskIdArtichoke-i",
            type: "Artichoke",
        });
        expect(res[1]).toEqual({
            id: "taskIdTomate-i",
            type: "Tomate",
        });
    });
});