import { itkResolver, itkResolverById } from "../../src/itk/api/resolverItk";

describe("itk query", () => {
    test("test-1 itkResolver return all list of itk", async () => {
        const repositoryItkMem = {
            getAll: async () => ([
                {
                    id: "Artichoke-i",
                    name: "Artichoke",
                    description: "Okay",
                    cultureMode: "cultureMode",
                    plant: "plant",
                    task: [
                        {
                            taskId: "taskId",
                            info: "info"
                        }
                    ]
                },
                {
                    id: "Tomate-i",
                    name: "Tomate",
                    description: "BOOMER",
                    cultureMode: "cultureMode1",
                    plant: "plant1",
                    task: []
                }
            ]),
            getItkById: async (id: string) => (undefined)
        }
        const res = await itkResolver(repositoryItkMem);

        expect(res).toHaveLength(2);
        expect(res[0]).toEqual({
            id: "Artichoke-i",
            name: "Artichoke",
            description: "Okay",
            cultureMode: "cultureMode",
            plant: "plant",
            task: [
                {
                    taskId: "taskId",
                    info: "info"
                }
            ]
        });
        expect(res[1]).toEqual({
            id: "Tomate-i",
            name: "Tomate",
            description: "BOOMER",
            cultureMode: "cultureMode1",
            plant: "plant1",
            task: []
        });
    })

    test("test-2 itkResolver return list of itk by id", async () => {
        const response = [{
            id: "Artichoke-i",
            name: "Artichoke",
            description: "Okay",
            cultureMode: "cultureMode",
            plant: "plant",
            task: [
                {
                    taskId: "taskId",
                    info: "info"
                }
            ]
        }];

        const repositoryItkMem = {
            getAll: async () => (response),
            getItkById: async (id: string) => (response.filter(itk => itk.id === id)[0])
        };

        const res = await itkResolverById(repositoryItkMem, "Artichoke-i");

        expect(res).toEqual({
            id: "Artichoke-i",
            name: "Artichoke",
            description: "Okay",
            cultureMode: "cultureMode",
            plant: "plant",
            task: [
                {
                    taskId: "taskId",
                    info: "info"
                }
            ]
        });
    });

    test("test-3 itkResolver return list of itk by id invalid", async () => {
        const repositoryItkMem = {
            getAll: async () => ([]),
            getItkById: async (id: string) => (undefined)
        };
        const res = await itkResolverById(repositoryItkMem, "foo");

        expect(res).toEqual({
            id: "",
            name: "",
            description: "",
            cultureMode: "",
            plant: "",
            task: []
        });
    });

    test("test-4 itkResolver return list of itk by id undifined", async () => {
        const repositoryItkMem = {
            getAll: async () => ([]),
            getItkById: async (id: string) => (undefined)
        };
        const res = await itkResolverById(repositoryItkMem, undefined);

        expect(res).toEqual({
            id: "",
            name: "",
            description: "",
            cultureMode: "",
            plant: "",
            task: []
        });
    });
});