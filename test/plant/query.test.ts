import { plantResolver, plantResolverById } from "../../src/plant/api/resolverPlant";


describe("plant query", () => {
    test("test-1 plantResolver return all list of plant", async () => {
        const repositoryPlantMem = {
            getAll: async () => ([
                {
                    id: "Artichoke-i",
                    name: "Artichoke",
                    description: "Okay"
                },
                {
                    id: "Tomate-i",
                    name: "Tomate",
                    description: "BOOMER"
                }
            ]),
            getPlantById: async (id: string) => (undefined)
        }
        const res = await plantResolver(repositoryPlantMem);

        expect(res).toHaveLength(2);
        expect(res[0]).toEqual({
            id: "Artichoke-i",
            name: "Artichoke",
            description: "Okay"
        });
        expect(res[1]).toEqual({
            id: "Tomate-i",
            name: "Tomate",
            description: "BOOMER"
        });
    });

    test("test-2 plantResolver return list of plant by id", async () => {
        const response = [{
            id: "Artichoke-i",
            name: "Artichoke",
            description: "Okay"
        }]

        const repositoryPlantMem = {
            getAll: async () => ([]),
            getPlantById: async (id: string) => (response.filter(res => res.id === id)[0])
        }

        const res = await plantResolverById(repositoryPlantMem, "Artichoke-i");

        expect(res).toEqual({
            id: "Artichoke-i",
            name: "Artichoke",
            description: "Okay"
        });
    });

    test("test-3 plantResolver return list of plant by id invalid", async () => {
        const repositoryPlantMem = {
            getAll: async () => ([]),
            getPlantById: async (id: string) => (undefined)
        }

        const res = await plantResolverById(repositoryPlantMem, "foo");

        expect(res).toEqual({
            id: "",
            name: "",
            description: ""
        });
    });

    test("test-4 plantResolver return list of plant by id undefined", async () => {
        const repositoryPlantMem = {
            getAll: async () => ([]),
            getPlantById: async (id: string) => (undefined)
        }

        const res = await plantResolverById(repositoryPlantMem, undefined);

        expect(res).toEqual({
            id: "",
            name: "",
            description: ""
        });
    });
});