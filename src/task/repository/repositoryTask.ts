import { config } from "../../config/env";
import { GraphDBClient, GraphProperty } from "../../graphDBClient";
import { task } from "../api/domain";

interface TaskGraph {
    taskId: GraphProperty;
    taskType: GraphProperty;
}

export class RepositoryTaskGraphDB {
    private dbClient: GraphDBClient;

    constructor() {
        this.dbClient = new GraphDBClient({
            config: {
                endpoint: config.GRAPHDB_ENDPOINT,
                username: config.GRAPHDB_USER,
                password: config.GRAPHDB_PASS,
                repository: config.GRAPHDB_REPOSITORY
            }
        });
    }

    async getAll(): Promise<task[]> {
        const res = await this.dbClient.query<TaskGraph>({
            query: `
            PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
            PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            select ?taskId ?taskType where { 
                ?taskId a c3pocm:TechnicalOperation .
                ?taskId c3pocm:implements /rdf:type ?taskType
                FILTER NOT EXISTS{?childType rdfs:subClassOf ?taskType}
            }
        `});

        return res.results.bindings.map(task => {
            return {
                id: task.taskId.value,
                type: task.taskType.value
            }
        });
    }
}