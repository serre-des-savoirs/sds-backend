import { RepositoryTask } from "./domain";
import { RepositoryTaskGraphDB } from "../repository/repositoryTask";

export async function taskResolver(repositoryTask: RepositoryTask) {
    return await repositoryTask.getAll();
}

export const resolverTask = {
    Query: {
        taskTypes: async () => taskResolver(new RepositoryTaskGraphDB()),
    },
};