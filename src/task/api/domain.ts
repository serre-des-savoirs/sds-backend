export interface task {
    id: string;
    type: string;
}

export interface RepositoryTask {
    getAll: () => Promise<task[]>;
}