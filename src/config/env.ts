export const config = {
    GRAPHDB_ENDPOINT: process.env.GRAPHDB_ENDPOINT || 'https://graph.dev.elzeard.co',
    GRAPHDB_USER: process.env.GRAPHDB_USER || 'guest',
    GRAPHDB_PASS: process.env.GRAPHDB_PASS || 'seedspwd!',
    GRAPHDB_REPOSITORY: process.env.GRAPHDB_REPOSITORY || 'sds-poc'
};