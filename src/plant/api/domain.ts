export type plant = {
    id: string;
    name: string;
    description: string;
}

export interface RepositoryPlant {
    getAll: () => Promise<plant[]>;
    getPlantById: (id: string) => Promise<plant | undefined>;
}