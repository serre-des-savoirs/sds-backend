import { GraphDBClient, GraphLabelProperty, GraphProperty } from "../../graphDBClient";
import { config } from "../../config/env";
import { plant } from "../api/domain";

interface PlantGraph {
    plant: GraphProperty;
    plantName: GraphLabelProperty;
}

export class RepositoryPlantGraphDB {
    private dbClient: GraphDBClient;
    constructor() {
        this.dbClient = new GraphDBClient({
            config: {
                endpoint: config.GRAPHDB_ENDPOINT,
                username: config.GRAPHDB_USER,
                password: config.GRAPHDB_PASS,
                repository: config.GRAPHDB_REPOSITORY
            }
        });
    }

    async getAll(): Promise<plant[]> {
        const res = await this.dbClient.query<PlantGraph>({
            query: `
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX c3poplant: <http://www.elzeard.co/ontologies/c3po/plant#>
            select ?plant ?plantName where { 
                ?plant a c3poplant:CultivatedPlant .
                ?plant skos:prefLabel ?plantName .
                FILTER(LANGMATCHES(LANG(?plantName), 'fr'))
            }`
        });

        return res.results.bindings.map(plant => {
            return {
                id: plant.plant.value,
                name: plant.plantName.value,
                description: "Aucune description",
            }
        });
    }

    async getPlantById(id: string): Promise<plant | undefined> {
        const res = await this.dbClient.query<PlantGraph>({
            query: `
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
            select ?plantName where {
                c3pokb:${id} skos:prefLabel ?plantName .
                FILTER(LANGMATCHES(LANG(?plantName), 'fr'))
            }`
        });

        if (res.results.bindings.length === 0) return undefined;

        return {
            id: id,
            name: res.results.bindings[0].plantName.value,
            description: "Aucune description",
        }
    }
}