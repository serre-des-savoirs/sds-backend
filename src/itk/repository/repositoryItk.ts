import { GraphDBClient, GraphLabelProperty, GraphProperty } from "../../graphDBClient";
import { RepositoryItk, itk, task } from "../api/domain";
import { config } from "../../config/env";


interface ItkGraph {
    itk: GraphProperty;
    itkName: GraphLabelProperty;
    cultureMode: GraphProperty;
    cultureModeName: GraphLabelProperty;
    plant: GraphProperty;
    plantName: GraphLabelProperty;
    task?: GraphProperty;
}


export class RepositoryItkGraphDB implements RepositoryItk {
    private dbClient: GraphDBClient;

    constructor() {
        this.dbClient = new GraphDBClient({
            config: {
                endpoint: config.GRAPHDB_ENDPOINT,
                username: config.GRAPHDB_USER,
                password: config.GRAPHDB_PASS,
                repository: config.GRAPHDB_REPOSITORY
            }
        });
    }

    async getAll(): Promise<itk[]> {
        const res = await this.dbClient.query<ItkGraph>({
            query: `
            PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX dc: <http://purl.org/dc/elements/1.1/>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX c3povoc: <http://www.elzeard.co/ontologies/c3po/vocabulary#>
            select ?itk ?itkName ?cultureMode ?cultureModeName ?plant ?plantName where { 
                ?itk a c3pocm:CropItinerary .
                ?itk rdfs:label ?itkName .
                ?itk c3pocm:concernsPlant ?plant .
                ?plant skos:prefLabel ?plantName .
                ?itk c3pocm:hasCultureMode ?cultureMode .
                ?cultureMode skos:prefLabel ?cultureModeName .
                FILTER(LANGMATCHES(LANG(?itkName), 'fr'))
                FILTER(LANGMATCHES(LANG(?plantName), 'fr'))
                FILTER(LANGMATCHES(LANG(?cultureModeName), 'fr'))
            }`
        });

        return res.results.bindings.map(itk => {
            return {
                id: itk.itk.value,
                name: itk.itkName.value,
                description: "Aucune description",
                cultureMode: itk.cultureModeName.value,
                plant: itk.plant.value,
                task: []
            }
        });
    }

    async getItkById(id: string): Promise<itk | undefined> {
        const res = await this.dbClient.query<ItkGraph>({
            query: `
            PREFIX c3pocm: <http://www.elzeard.co/ontologies/c3po/cropManagement#>
            PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
            PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
            PREFIX c3pokb: <http://www.elzeard.co/ontologies/c3pokb#>
            select ?itkName ?cultureMode ?cultureModeName ?plant ?plantName ?task where {
                c3pokb:${id} rdfs:label ?itkName .
                c3pokb:${id} c3pocm:concernsPlant ?plant .
                ?plant skos:prefLabel ?plantName .
                c3pokb:${id} c3pocm:hasCultureMode ?cultureMode .
                c3pokb:${id} c3pocm:hasOperationMember ?task .
                ?cultureMode skos:prefLabel ?cultureModeName .
                FILTER(LANGMATCHES(LANG(?itkName), 'fr'))
                FILTER(LANGMATCHES(LANG(?plantName), 'fr'))
                FILTER(LANGMATCHES(LANG(?cultureModeName), 'fr'))
            }`
        });

        if (res.results.bindings.length === 0) return undefined;

        const taskList: task[] = res.results.bindings.filter(task => {
            return task.task !== undefined;
        }).map(task => {
            return {
                taskId: task.task ? task.task.value.split('#')[1] : "",
                info: "Aucune information"
            }
        });

        return {
            id: id,
            name: res.results.bindings[0].itkName.value,
            description: "Aucune description",
            cultureMode: res.results.bindings[0].cultureModeName.value,
            plant: res.results.bindings[0].plant.value,
            task: taskList
        }
    }
}
