import { RepositoryItk } from "./domain";
import { RepositoryItkGraphDB } from "../repository/repositoryItk";

export async function itkResolver(repositoryItk: RepositoryItk) {
    return await repositoryItk.getAll();
}

const emptyItk = {
    id: "",
    name: "",
    description: "",
    cultureMode: "",
    plant: "",
    task: []
}

export async function itkResolverById(repositoryItk: RepositoryItk, id: string | undefined) {
    if (id === undefined)
        return (emptyItk);
    const res = await repositoryItk.getItkById(id);
    return res || emptyItk;
}

export const resolverItk = {
    Query: {
        itks: () => itkResolver(new RepositoryItkGraphDB()),
        getItk: async (parent: unknown, args: { id: string | undefined }) => itkResolverById(new RepositoryItkGraphDB(), args.id)
    },
};