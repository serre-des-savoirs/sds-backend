export type itk = {
    id: string;
    name: string;
    description: string;
    cultureMode: string;
    plant: string;
    task: task[];
}

export interface task {
    taskId: string;
    info: string;
}

export interface RepositoryItk {
    getAll: () => Promise<itk[]>;
    getItkById: (id: string) => Promise<itk | undefined>;
}