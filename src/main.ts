import { createYoga } from 'graphql-yoga'
import { createServer } from 'http'
import { gatewaySchema } from './schema/schema'
require('dotenv').config()

async function main() {
    const yoga = createYoga({ 
        schema: gatewaySchema,
    })
    const server = createServer(yoga)
    server.on('error', (error) => {
        console.error(error)
    })

    server.listen(3038, () => {
        console.info('Server is running on http://localhost:3038/graphql')
    })
}

main();