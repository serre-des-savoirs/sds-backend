import got from 'got';
import log from 'loglevel';

const NAMED_GRAPH_PREFIX_URI = 'http://www.elzeard.co/graphs/c3pokb';

export const PLANTS_NAMED_GRAPH_URI = `${NAMED_GRAPH_PREFIX_URI}/plants`;
export const INPUTS_NAMED_GRAPH_URI = `${NAMED_GRAPH_PREFIX_URI}/inputs`;

export interface GraphDBConfig {
  endpoint: string;
  username: string;
  password: string;
  repository: string;
}

export interface GraphQueryResponse<T> {
  head: {
    vars: string[];
  };
  results: {
    bindings: T[];
  };
}

export interface GraphProperty {
  type: string;
  value: string;
}

export interface GraphLabelProperty extends GraphProperty {
  ['xml:lang']: string;
}

export interface GraphDataProperty extends GraphProperty {
  datatype: string;
}

export class GraphDBClient {
  private endpoint: string;
  private username: string;
  private password: string;
  private repository: string;

  constructor({ config }: { config: GraphDBConfig }) {
    this.endpoint = config.endpoint;
    this.username = config.username;
    this.password = config.password;
    this.repository = config.repository;
  }

  async query<T>({ query, asJsonLd }: { query: string; asJsonLd?: boolean }): Promise<GraphQueryResponse<T>> {
    let headers: { Accept?: string } = {};

    if (asJsonLd) {
      headers['Accept'] = 'application/ld+json';
    }

    try {
      const response: { body: Promise<GraphQueryResponse<T>> } = await got.post(
        `${this.endpoint}/repositories/${this.repository}`,
        {
          username: this.username,
          password: this.password,
          headers,
          form: {
            query,
            infer: true,
            sameAs: true,
          },
          responseType: 'json',
        },
      );
      return response.body;
    } catch (e: any) {
      log.error(`Received error "${e.message}" for following query : (check graphDB logs !)`);
      log.error('====');
      log.error(query);
      log.error('====\n');
      throw e;
    }
  }

  async update({ update }: { update: string }) {
    try {
      const response = await got.post(`${this.endpoint}/repositories/${this.repository}/statements`, {
        username: this.username,
        password: this.password,
        form: {
          update,
          infer: true,
          sameAs: true,
        },
        responseType: 'json',
      });
      return response.body;
    } catch (e: any) {
      log.error(`Received error "${e.message}" for following update query (check graphDB logs !)`);
      log.error(update);
      throw e;
    }
  }
}